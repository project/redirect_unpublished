CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Credits

INTRODUCTION
------------

This module provides a redirection to custom URL to unpublished nodes, if visited. Provides a choice among 301 and 302 redirect.
Permission can be set for any role to administer this module.

INSTALLATION
------------

1. Install and enable the module as usual.
2. Navigate to "admin/config/system/redirect-unpublished" and configure URL and type of redirection.


CREDITS
-------
* Pranay Bhardwaj (pranay2284)
